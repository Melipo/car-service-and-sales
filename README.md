# CarCar

Team:

- Alex - Sales
- Missy - Service

## Design

The overall design of our project consisted of a Service Microservice, Sales Microservice and an Inventory Microservice. On the front-end, we divided up the navigation bar to distinguish between which forms and/or list views pertained to each individual microservice. You can also access each microservice directly from the Main Page. There is no importing of code between microservices. We practiced consistent naming across the application, ensured that each component on the front end was in a separate file, inserted error messages when appropriate and also added alerts on the front-end so that users would know when forms were successfully created for the Sales and Service Microservices. In the Inventory front-end, users can view models, manufacturers and automobiles. They can also create a manufacturer, model and automobile. Users can also see pictures of the models that have been uniformally sized for optimal viewing.

## Service microservice

I created three different models for the Service Microservice; Technician, Appointment,and and AutoVO model that polled from the Inventory API to extract just the vin for each automobile. On the back end, I designed CRUD functionality for all model instances. On the front end, users can create a Technician and create an appointment.  Users can also see a list of all appointments, which includes a cancel button to cancel the appointment and a complete button to mark the repair as complete. When an appointment is created with a VIN that matches inventory, it marks the customer as a VIP. Both buttons will remove the appointment from the list. Additionally, users can filter service history by VIN.

## Sales microservice

I created four different models for the Sales Microservice, which included Customer, Sales Person, Sale and an AutomobileVO model. The AutomobileVO model was set up to poll from the Inventory API to extract the vin for each automobile and its respective href. I also created a function to display the automobile VOs to ensure they were polling and displayed properly in Insomnia. On the back end, I designed CRUD functionality for all model instances, including Customers, Employees and Sale Records. On the front end, users can create Customers, Employees and Sales Records, as well. Users can also see a list of all sales, which includes a functioning delete button. Additionally, users can filter the sales person history list by sales person. This includes a drop down list of all active employees and a search bar to filter the sales list by that employee.
