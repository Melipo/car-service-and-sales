# Generated by Django 4.0.3 on 2022-12-11 21:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("sales_rest", "0002_sale_is_sold"),
    ]

    operations = [
        migrations.AlterField(
            model_name="sale",
            name="automobile",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="automobiles",
                to="sales_rest.automobilevo",
            ),
        ),
        migrations.AlterField(
            model_name="sale",
            name="customer",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="customers",
                to="sales_rest.customer",
            ),
        ),
        migrations.AlterField(
            model_name="sale",
            name="employee",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="employees",
                to="sales_rest.employee",
            ),
        ),
    ]
