from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100)
    vin = models.CharField(max_length=17)

    def __str__(self):
        return f"{self.vin}"


class Employee(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField(default=0000)

    def get_api_url(self):
        return reverse("list_employee", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.name}"


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("list_customer", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.name}"


class Sale(models.Model):
    employee = models.ForeignKey(
        Employee,
        related_name="employees",
        on_delete=models.PROTECT,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.PROTECT,
    )
    price = models.IntegerField()
    is_sold = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("show_sale", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.customer} - {self.automobile} - {self.price} - {self.employee} - {self.is_sold}"
