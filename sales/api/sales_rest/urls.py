from django.urls import path
from .views import (
    list_sales,
    show_sale,
    list_employees,
    show_employee,
    list_customer,
    show_customer,
    list_automobile_vo,
)

urlpatterns = [
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:pk>/", show_sale, name="show_sale"),
    path("employees/", list_employees, name="list_employees"),
    path("employees/<int:pk>/", show_employee, name="show_employee"),
    path("customers/", list_customer, name="list_customer"),
    path("customers/<int:pk>/", show_customer, name="show_customer"),
    path("automobile/", list_automobile_vo, name="list_automobile_vo"),
]
