from .models import AutomobileVO, Employee, Sale, Customer
from common.json import ModelEncoder

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

class EmployeeEncoder(ModelEncoder):
    model = Employee
    properties = [
        "id",
        "name",
        "employee_number",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "is_sold",
        "price",
        "customer",
        "employee",
        "automobile",
        "id",
    ]
    encoders = {
        "employee": EmployeeEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "vin": o.automobile.vin,
        }
