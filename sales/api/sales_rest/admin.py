from django.contrib import admin

from .models import AutomobileVO, Customer, Employee, Sale

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass

@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    pass

@admin.register(Sale)
class SaleRecordAdmin(admin.ModelAdmin):
    pass
