from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .models import AutomobileVO, Employee, Sale, Customer
from .encoder import (
    AutomobileVOEncoder,
    CustomerEncoder,
    EmployeeEncoder,
    SaleEncoder,
    )

@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            automobile = AutomobileVO.objects.get(import_href=content["automobile"])
            content["automobile"] = automobile
            employee = Employee.objects.get(employee_number=content["employee"])
            content["employee"] = employee
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
            sale = Sale.objects.create(**content)
            sale.is_sold = True
            sale.save()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sale"}
                )
            response.status_code = 404
            return response
        

@require_http_methods(["DELETE", "GET", "PUT"])
def show_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(sale, encoder=SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Sale record does not exist"}
                )
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.is_sold = False
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Sale record does not exist"}
                )
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            sale = Sale.objects.get(id=pk)
            props = ["price"]
            for prop in props:
                if prop in content:
                    setattr(sale, prop, content[prop])
            sale.save()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Sale record does not exist"}
                )
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_employees(request):
    if request.method == "GET":
        employees = Employee.objects.all()
        return JsonResponse(
            {"employees": employees},
            encoder=EmployeeEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            employee = Employee.objects.create(**content)
            return JsonResponse(
                employee,
                encoder=EmployeeEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Employee could not be created"}
                )
            response.status_code = 404
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def show_employee(request, pk):
    if request.method == "GET":
        try:
            employee = Employee.objects.get(id=pk)
            return JsonResponse(
                employee, 
                encoder=EmployeeEncoder, 
                safe=False)
        except Employee.DoesNotExist:
            response = JsonResponse(
                {"message": "Employee does not exist"}
                )
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            employee = Employee.objects.get(id=pk)
            employee.delete()
            return JsonResponse(
                employee,
                encoder=SaleEncoder,
                safe=False,
            )
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Employee does not exist"}
                )
    else:
        try:
            content = json.loads(request.body)
            employee = Employee.objects.get(id=pk)
            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(employee, prop, content[prop])
            employee.save()
            return JsonResponse(
                employee,
                encoder=EmployeeEncoder,
                safe=False,
            )
        except Employee.DoesNotExist:
            response = JsonResponse(
                {"message": "Employee does not exist"}
                )
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
        except:
            response = JsonResponse(
                {"message": "Customer could not be created"}
                )
            response.status_code = 404
            return response
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_customer(request, pk):
    if request.method == "GET":
        try:
            employee = Customer.objects.get(id=pk)
            return JsonResponse(
                employee, 
                encoder=CustomerEncoder, 
                safe=False)
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"}
                )
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=Customer,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"}
                )
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)
            props = ["name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"}
                )
            response.status_code = 404
            return response


def list_automobile_vo(request):
    automobile_vo = AutomobileVO.objects.all()
    return JsonResponse(
        automobile_vo, 
        encoder=AutomobileVOEncoder, 
        safe=False)
