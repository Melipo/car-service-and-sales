from common.json import ModelEncoder
from .models import AutoVO, Technician, Appointment

class TechEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_num",
    ]

class AutoVOEncoder(ModelEncoder):
    model = AutoVO
    properties = [
        "id",
        "vin",
    ]

class ApptEncoder(ModelEncoder):
    model =Appointment
    properties = [
        "id",
        "vin",
        "customer",
        "date_time",
        "reason",
        "status",
        "vip",
        "technician",
    ]

    encoders = {"technician": TechEncoder()}
