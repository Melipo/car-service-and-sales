from django.urls import path
from .views import api_list_appt, api_tech, api_show_tech, api_show_appt

urlpatterns = [
    path("appointments/", api_list_appt, name="api_list_appt"),
    path("technicians/", api_tech, name="api_tech"),
    path("technicians/<int:id>/", api_show_tech, name="api_tech"),
    path("appointments/<int:id>/", api_show_appt, name="api_show_appt"),
]