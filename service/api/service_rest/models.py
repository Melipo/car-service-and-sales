from django.db import models
from django.urls import reverse


class Technician(models.Model):
    name = models.CharField(max_length=75)
    employee_num = models.IntegerField()

    def __str__(self):
        return f"{self.name}"

    def get_api_url(self):
        return reverse("list_technician", kwargs={"id": self.id})

class Appointment(models.Model):
    vin = models.CharField(max_length=50, null=True, blank=True)
    customer = models.CharField(max_length=100, null=True, blank=True)
    date_time = models.DateTimeField(null=True, blank=True)
    reason = models.CharField(max_length=500, null=True, blank=True)
    status = models.BooleanField(default=False, null=True, blank=True)
    vip = models.BooleanField(default=False, null=True, blank=True)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return f"{self.customer} - {self.date_time} - {self.technician}"

class AutoVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


    def get_api_url(self):
        return reverse("api_autoVO", kwargs={"vin": self.vin})
