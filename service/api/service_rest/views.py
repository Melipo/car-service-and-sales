from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import AutoVO, Technician, Appointment
from .encoders import TechEncoder, ApptEncoder


@require_http_methods(["GET", "POST"])
def api_tech(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechEncoder
            )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechEncoder,
                safe=False,
                )
        except:
            response = JsonResponse(
                {"message": "Technician could not be created."}
            )
            response.status_code = 404
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_tech(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(
                technician,
                encoder=TechEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Technician does not exist"}
                )
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.filter(id=id)
            technician.delete()
            return JsonResponse(
                {"message": "Technician has been removed from the system."}
            )
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Technician does not exist."}
                )
            response.status_code = 404
            return response
    else:
        content = json.loads(request.body)
        try:
            Technician.objects.filter(id=id).update(**content)
            technician = Technician.objects.get(id=id)
            return JsonResponse(
                technician,
                encoder=TechEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse (
                {"message": "Technician not ths system."}
                )
            response.status_code = 404
            return response



@require_http_methods(["GET", "POST"])
def api_list_appt(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=ApptEncoder
            )
    else:
        try:
            content = json.loads(request.body)
            technician_id = content["technician_id"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician

            try:
                automobile = AutoVO.objects.get(vin=content["vin"])
                content["vip"] = True
            except:
                pass


            appointment = Appointment.objects.create(**content)
            print("appointment" + appointment)
            return JsonResponse(
                appointment,
                encoder=ApptEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Appointment could not be created."}
                )
            response.status_code = 404
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appt(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=ApptEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Appointment not in the system."}
                )
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            count, _ = Appointment.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0}
                )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment has been deleted."}
                )
    else:
        content = json.loads(request.body)
        try:
            Appointment.objects.filter(id=id).update(**content)
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=ApptEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse (
                {"message": "Appointment not ths system."}
                )
            response.status_code = 404
            return response
