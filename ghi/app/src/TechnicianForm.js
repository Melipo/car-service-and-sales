import React from "react";

class TechnicianForm extends React.Component {
  state = {
    name: "",
    employee_num: "",
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name: this.state.name,
      employee_num: this.state.employee_num,
    };

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);

    if (response.ok) {
      const newCustomer = await response.json();
      console.log(newCustomer);

      const cleared = {
        name: "",
        employee_num: "",
      };
      this.setState(cleared);
      alert(
        `${this.state.name} has been added to the system. Employee ID: ${this.state.employee_num}`
      );
    }
  };

  handleInputChange = (event) => {
    const value = event.target.value;
    this.setState({ [event.target.name]: value });
  };

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Enter a New Technician</h1>
            <form onSubmit={this.handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.name}
                  placeholder="name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.employee_num}
                  placeholder="employee_num"
                  required
                  type="text"
                  name="employee_num"
                  id="employee_num"
                  className="form-control"
                />
                <label htmlFor="employee_num">Employee Number</label>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default TechnicianForm;
