import { useState, useEffect } from "react";

function SalesList() {
  const [sales, setSales] = useState([]);
  const getData = async () => {
    const resp = await fetch("http://localhost:8090/api/sales/");
    const data = await resp.json();
    setSales(data.sales);
  };

  const handleDelete = async (id) => {
    const resp = await fetch(`http://localhost:8090/api/sales/${id}`, {
      method: "DELETE",
    });
    const data = await resp.json();
    window.location = "/sales";
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Sales Person</th>
          <th>Employee Number</th>
          <th>Customer Name</th>
          <th>Price</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {sales.map((sale) => {
          return (
            <tr key={sale.id}>
              <td>{sale.employee.name}</td>
              <td>{sale.employee.employee_number}</td>
              <td>{sale.customer.name}</td>
              <td>${sale.price}</td>
              <td>
                <button
                  className="btn btn-primary m-2"
                  onClick={() => {
                    handleDelete(sale.id);
                  }}
                >
                  Delete
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default SalesList;
