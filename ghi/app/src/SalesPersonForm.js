import React from "react";

class SalesPersonForm extends React.Component {
  state = {
    name: "",
    employee_number: "",
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name: this.state.name,
      employee_number: this.state.employee_number,
    };

    const employeeUrl = "http://localhost:8090/api/employees/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(employeeUrl, fetchConfig);
    if (response.ok) {
      const newEmployee = await response.json();
      console.log(newEmployee);

      const cleared = {
        name: "",
        employee_number: "",
      };
      this.setState(cleared);
      alert(`${this.state.name} has been added to the system.`);
    }
  };

  handleInputChange = (event) => {
    const value = event.target.value;
    this.setState({ [event.target.name]: value });
  };

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Sales Person</h1>
            <form onSubmit={this.handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.name}
                  placeholder="name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.employee_number}
                  placeholder="employee_number"
                  required
                  type="number"
                  name="employee_number"
                  id="employee_number"
                  className="form-control"
                />
                <label htmlFor="employee_number">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default SalesPersonForm;
