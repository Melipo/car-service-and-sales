import { useState, useEffect } from "react";

function ApptList() {
  const [appointments, setAppointments] = useState([]);

  const getData = async () => {
    const resp = await fetch("http://localhost:8080/api/appointments/");
    const data = await resp.json();

    setAppointments(data.appointments);
  };

  const handleCancel = async (id) => {
    const resp = await fetch(`http://localhost:8080/api/appointments/${id}`, {
      method: "DELETE",
    });
    const data = await resp.json();
    window.location = "/appointments";
  };

  const handleComplete = async (id) => {
    const resp = await fetch(`http://localhost:8080/api/appointments/${id}`, {
      method: "DELETE",
    });
    const data = await resp.json();
    window.location = "/appointments";
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <h1>Upcoming Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer</th>
            <th>Date of Appointment</th>
            <th>Time of Appointment</th>
            <th>Reason</th>
            <th>VIP Status</th>
            <th>Technician</th>
            <th>Cancel Appointment</th>
            <th>Mark Repair as Complete</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date_time.slice(0, 10)}</td>
                <td>{appointment.date_time.slice(11, 16)}</td>
                <td>{appointment.reason}</td>
                <td>
                  {appointment.vip === true
                    ? "Customer is a VIP!"
                    : "Customer is not a VIP"}
                </td>
                <td>{appointment.technician.name}</td>
                <td>
                  <button
                    className="btn btn-danger m-2"
                    onClick={() => {
                      handleCancel(appointment.id);
                    }}
                  >
                    CANCEL
                  </button>
                </td>
                <td>
                  <button
                    className="btn btn-success m-2"
                    onClick={() => {
                      handleComplete(appointment.id);
                    }}
                  >
                    Completed
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ApptList;
