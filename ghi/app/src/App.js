import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import ServiceHistory from './ServiceHistory';
import TechnicianForm from './TechnicianForm';
import ApptList from './ApptList';
import ApptForm from './ApptForm';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import SalesList from './SalesList';
import SalesPersonForm from './SalesPersonForm';
import SalesPersonHistoryList from './SalesPersonHistoryList';
import CustomerForm from './CustomerForm';
import CreateSaleForm from './CreateSaleForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="service/history" element={<ServiceHistory service={props.service} />} />
          <Route path="appointments" element={<ApptList appointments={props.appointments} />} />
          <Route path="appointments/new" element={<ApptForm />} />
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="manufacturers" element={<ManufacturerList manufacturers={props.manufacturers} />} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="models" element={<ModelList models={props.models} />} />
          <Route path="models/new" element={<ModelForm />} />
          <Route path="automobiles" element={<AutomobileList autos={props.autos} />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />
          <Route path="sales" element={<SalesList sales={props.sales} />} />
          <Route path="sales/new" element={<CreateSaleForm sales={props.sales} />} />
          <Route path="sales/history" element={<SalesPersonHistoryList sales={props.sales} />} />
          <Route path="employees/new" element={<SalesPersonForm />} />
          <Route path="customers/new" element={<CustomerForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
