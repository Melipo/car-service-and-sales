import React from "react";

class AutomobileForm extends React.Component {
  state = {
    color: "",
    year: "",
    vin: "",
    model_id: "",
    models: [],
  };

  async componentDidMount() {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      color: this.state.color,
      year: this.state.year,
      vin: this.state.vin,
      model_id: this.state.model_id,
    };

    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      const newAutomobile = await response.json();
      console.log(newAutomobile);

      const cleared = {
        color: "",
        year: "",
        vin: "",
        model_id: "",
      };
      this.setState(cleared);
      alert(
        `VIN ${this.state.vin} has been added to inventory. The car can now be sold.`
      );
    }
  };

  handleInputChange = (event) => {
    const value = event.target.value;
    this.setState({ [event.target.name]: value });
  };

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an Automobile to Inventory</h1>
            <form onSubmit={this.handleSubmit} id="create-automobile-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.color}
                  placeholder="color"
                  required
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.year}
                  placeholder="year"
                  required
                  type="number"
                  name="year"
                  id="year"
                  className="form-control"
                />
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.vin}
                  placeholder="vin"
                  required
                  type="text"
                  name="vin"
                  id="vin"
                  className="form-control"
                />
                <label htmlFor="vin">Vin</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  value={this.state.model_id}
                  required
                  id="model_id"
                  name="model_id"
                  className="form-select"
                >
                  <option value="">Choose a Model</option>
                  {this.state.models.map((model) => {
                    return (
                      <option key={model.id} value={model.id}>
                        {model.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default AutomobileForm;
