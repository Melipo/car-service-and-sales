import { NavLink } from "react-router-dom";
import "./Nav.css";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">
                Home
              </NavLink>
            </li>

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                Sales
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown" to="/sales">
                    List of Sales
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="/sales/new">
                    Create a Sale
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="/sales/history">
                    Sales Person History
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="employees/new">
                    Add a Sales Person
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="customers/new">
                    Add a Customer
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                Service
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="/appointments">
                    Appointments
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link-dropdown "
                    to="/appointments/new"
                  >
                    Create an Appointment
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="/service/history">
                    Service History
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                Inventory
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="/manufacturers">
                    Manufacturers
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link-dropdown "
                    to="/manufacturers/new"
                  >
                    Create a Manufacturer
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="/models">
                    Vehicle Models
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="/models/new">
                    Create a Vehicle Model
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="/automobiles">
                    Automobiles
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link-dropdown " to="/automobiles/new">
                    Create an Automobile
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
