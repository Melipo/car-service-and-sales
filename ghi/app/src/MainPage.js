function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Anley and Pompeo Sales and Services</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership management!
        </p>
        <br></br>
        <br></br>
        <div>
          <h3>Take me to the Service Department</h3>
          <a href="http://localhost:3000/service/history">
            <img
              src="https://static.thenounproject.com/png/963363-200.png"
              width="150"
            />
          </a>
          <h3>Take me to the Sales Department</h3>
          <br></br>
          <a href="http://localhost:3000/sales/history">
            <img
              src="https://cdn-icons-png.flaticon.com/512/2155/2155856.png"
              width="100"
            />
          </a>
          <br></br>
          <br></br>
          <br></br>
          <h3>Take me to Inventory</h3>
          <a href="http://localhost:3000/automobiles">
            <img
              src="https://static.thenounproject.com/png/1301599-200.png"
              width="100"
            />
          </a>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
