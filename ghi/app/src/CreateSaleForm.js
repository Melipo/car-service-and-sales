import React from "react";

class CreateSaleForm extends React.Component {
  state = {
    automobile: "",
    employee: "",
    customer: "",
    price: "",
    automobiles: [],
    employees: [],
    customers: [],
  };

  async componentDidMount() {
    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const automobileResponse = await fetch(automobileUrl);

    if (automobileResponse.ok) {
      const data = await automobileResponse.json();
      this.setState({ autos: data.autos });
    }
    const employeeUrl = "http://localhost:8090/api/employees/";
    const employeeResponse = await fetch(employeeUrl);

    if (employeeResponse.ok) {
      const data = await employeeResponse.json();
      this.setState({ employees: data.employees });
    }
    const customerUrl = "http://localhost:8090/api/customers/";
    const customerResponse = await fetch(customerUrl);

    if (customerResponse.ok) {
      const data = await customerResponse.json();
      this.setState({ customers: data.customers });
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      automobile: this.state.automobile,
      employee: this.state.employee,
      customer: this.state.customer,
      price: this.state.price,
    };

    const salesUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(salesUrl, fetchConfig);
    if (response.ok) {
      const newSale = await response.json();
      console.log(newSale);

      const cleared = {
        automobile: "",
        employee: "",
        customer: "",
        price: "",
      };
      this.setState(cleared);
      alert(`Congratulations! Sale has been created.`);
    }
  };

  handleInputChange = (event) => {
    const value = event.target.value;
    this.setState({ [event.target.name]: value });
  };

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a New Sale</h1>
            <form onSubmit={this.handleSubmit} id="create-sale-form">
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  value={this.state.automobile}
                  required
                  id="automobile"
                  name="automobile"
                  className="form-select"
                >
                  <option value="">Choose an Automobile</option>
                  {this.state.autos?.map((auto) => {
                    return (
                      <option key={auto.href} value={auto.href}>
                        {auto.vin}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  value={this.state.employee}
                  required
                  id="employee"
                  name="employee"
                  className="form-select"
                >
                  <option value="">Choose an Employee</option>
                  {this.state.employees.map((employee) => {
                    return (
                      <option
                        key={employee.employee_number}
                        value={employee.employee_number}
                      >
                        {employee.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  value={this.state.customer}
                  required
                  id="customer"
                  name="customer"
                  className="form-select"
                >
                  <option value="">Choose a Customer</option>
                  {this.state.customers.map((customer) => {
                    return (
                      <option key={customer.id} value={customer.id}>
                        {customer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.price}
                  placeholder="price"
                  required
                  type="number"
                  name="price"
                  id="price"
                  className="form-control"
                />
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default CreateSaleForm;
