import React from "react";

class ApptForm extends React.Component {
  state = {
    vin: "",
    customer: "",
    date_time: "",
    reason: "",
    status: false,
    technician_id: "",
    technician: [],
  };

  async componentDidMount() {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ technician: data.technician });
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      vin: this.state.vin,
      customer: this.state.customer,
      date_time: this.state.date_time,
      reason: this.state.reason,
      technician_id: this.state.technician_id,
    };

    alert(
      `Appointment for ${this.state.customer} has been created for ${this.state.date_time}.`
    );

    const apptUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(apptUrl, fetchConfig);
    if (response.ok) {
      const newAppt = await response.json();
      console.log(newAppt);

      const cleared = {
        vin: "",
        customer: "",
        date_time: "",
        reason: "",
        technician_id: "",
      };
      this.setState(cleared);
    }
  };

  handleInputChange = (event) => {
    const value = event.target.value;
    this.setState({ [event.target.name]: value });
  };

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Appointment</h1>
            <br></br>
            <form onSubmit={this.handleSubmit} id="create-appt-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.vin}
                  placeholder="vin"
                  required
                  type="text"
                  name="vin"
                  id="vin"
                  className="form-control"
                />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.customer}
                  placeholder="customer"
                  required
                  type="text"
                  name="customer"
                  id="customer"
                  className="form-control"
                />
                <label htmlFor="customer">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.date_time}
                  placeholder="date_time"
                  required
                  type="datetime-local"
                  name="date_time"
                  id="date_time"
                  className="form-control"
                />
                <label htmlFor="customer">
                  Enter the day and time of the appointmnet.
                </label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.reason}
                  placeholder="reason"
                  required
                  type="text"
                  name="reason"
                  id="reason"
                  className="form-control"
                />
                <label htmlFor="reason">
                  Enter the reason for the appointment.
                </label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  value={this.state.technician_id}
                  required
                  id="technician_id"
                  name="technician_id"
                  className="form-select"
                >
                  <option value="">Choose a Technician</option>
                  {this.state.technician.map((technician) => {
                    return (
                      <option key={technician.id} value={technician.id}>
                        {technician.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default ApptForm;
