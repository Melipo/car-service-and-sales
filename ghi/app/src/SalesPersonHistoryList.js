import { useState, useEffect } from "react";

function SalesHistory() {
  const [sales, setSales] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [employees, setEmployees] = useState([]);

  const getData = async () => {
    const resp = await fetch("http://localhost:8090/api/sales/");
    const data = await resp.json();
    const employeeResp = await fetch("http://localhost:8090/api/employees/");
    const employeeData = await employeeResp.json();
    setEmployees(employeeData.employees);

    setSales(data.sales);
  };
  const handleDelete = async (id) => {
    const resp = await fetch(`http://localhost:8090/api/sales/${id}`, {
      method: "DELETE",
    });
    const data = await resp.json();
    window.location = "/sales/history";
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <div>
        <h1>Sales History</h1>
      </div>
      <select>
        {employees.map((employee) => {
          return <option key = {employee.id} value={employee.number}>{employee.name}</option>;
        })}
      </select>
      <div>
        <input
          className="form-control"
          type="text"
          placeholder="Type Employee Name to Filter List"
          onChange={(event) => {
            setSearchTerm(event.target.value);
          }}
        />
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {sales
            .filter((sale) => {
              if (searchTerm === "") {
                return sale;
              } else if (sale.employee.name.includes(searchTerm)) {
                return sale;
              }
            })
            .map((sale) => {
              return (
                <tr key={sale.id}>
                  <td>{sale.employee.name}</td>
                  <td>{sale.customer.name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>${sale.price}</td>
                  <td>
                    <button
                      className="btn btn-primary m-2"
                      onClick={() => {
                        handleDelete(sale.id);
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </>
  );
}

export default SalesHistory;
