import { useState, useEffect } from "react";
function AutomobileList() {
  const [autos, setAutos] = useState([]);

  const getData = async () => {
    const resp = await fetch(`http://localhost:8100/api/automobiles/`);
    const data = await resp.json();

    setAutos(data.autos);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Vin</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
        </tr>
      </thead>
      <tbody>
        {autos.map((auto) => {
          return (
            <tr key={auto.href}>
              <td>{auto.vin}</td>
              <td>{auto.color}</td>
              <td>{auto.year}</td>
              <td>{auto.model.name}</td>
              <td>{auto.model.manufacturer.name}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AutomobileList;
