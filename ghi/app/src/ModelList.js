import { useState, useEffect } from "react";

function ModelList() {
  const [models, setModels] = useState([]);
  const getData = async () => {
    const resp = await fetch("http://localhost:8100/api/models/");
    const data = await resp.json();
    setModels(data.models);
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {models.map((model) => {
          return (
            <tr key={model.id}>
              <td>{model.name}</td>
              <td>{model.manufacturer.name}</td>
              <td>
                <img width="200" src={model.picture_url} />
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ModelList;
