import { useState, useEffect } from "react";

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");

  const getData = async () => {
    const resp = await fetch("http://localhost:8080/api/appointments/");
    const data = await resp.json();

    setAppointments(data.appointments);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <div>
        <h1>Service History</h1>
      </div>
      <div>
        <input
          className="form-control"
          type="text"
          placeholder="Type in Full VIN to Filter List"
          onChange={(event) => {
            setSearchTerm(event.target.value);
          }}
        />
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer</th>
            <th>Date of Appointment</th>
            <th>Time of Appointment</th>
            <th>Reason</th>
            <th>VIP Status</th>
            <th>Technician</th>
          </tr>
        </thead>
        <tbody>
          {appointments
            .filter((appointment) => {
              if (searchTerm === "") {
                return appointment;
              } else if (appointment.vin.includes(searchTerm)) {
                return appointment;
              }
            })
            .map((appointment) => {
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.customer}</td>
                  <td>{appointment.date_time.slice(0, 10)}</td>
                  <td>{appointment.date_time.slice(11, 16)}</td>
                  <td>{appointment.reason}</td>
                  <td>
                    {String(appointment.vip) === true
                      ? "Customer is a VIP!"
                      : "Customer is not a VIP"}
                  </td>
                  <td>{appointment.technician.name}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </>
  );
}

export default ServiceHistory;
